(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.mainMenuOffset = {

    attach: function (context) {
      var $main_menu = $('#main-menu', context);

      $main_menu.css({
        bottom: -($main_menu.height() / 2),
      });
    }

  };

  Drupal.behaviors.MainMenuDropDown = {

    attach: function (context) {
      $('#main-menu .menu-name-main-menu > .menu', context).westernDropdown();
    }

  };

}) (jQuery, Drupal, this, this.document);
