(function ($, window, document, undefined) {

  'use strict';

  var CLOSING = 'western-dropdown-closing';

  var OPENED = 'western-dropdown-opened';

  var OPENING = 'western-dropdown-opening';

  var $window = $(window);

  function click_submenu_parent(event) {
    var $target = event.currentTarget;

    if ($window.width() <= 800) {
      return false;
    }

    interrupt_animation($target);

    if ($target.hasClass(OPENED)) {
      close_submenu($target);
    }
    else {
      close_submenu($target.siblings('.' + OPENED));
      open_submenu($target);
    }
  }

  function click_submenu_parent_link() {
    if ($window.width() <= 800) {
      return false;
    }

    event.preventDefault();
  }

  function close_submenu($parent) {
    var $submenu = $parent.children('ul');

    if ($parent.hasClass(CLOSING)) {
      return false;
    }

    $parent.removeClass(OPENED);
    $parent.addClass(CLOSING);
    $submenu.css('zIndex', 9998);
    $submenu.fadeOut('slow', function () {
      $parent.removeClass(CLOSING);
    });
  }

  function interrupt_animation($parent) {
    $parent.children('ul').stop(true, true);
  }

  function mouseenter_submenu_parent(event) {
    var $target = $(event.currentTarget);

    if ($window.width() <= 800) {
      return false;
    }

    interrupt_animation($target);

    if (!$target.hasClass(OPENED)) {
      close_submenu($target.siblings('.' + OPENED));
      open_submenu($target);
    }
  }

  function mouseleave_submenu_parent(event) {
    var $target = $(event.currentTarget);

    if ($window.width() <= 800) {
      return false;
    }

    interrupt_animation($target);

    if ($target.hasClass(OPENED)) {
      close_submenu($target);
    }
  }

  function open_submenu($parent) {
    var $submenu = $parent.children('ul');

    if ($parent.hasClass(OPENING)) {
      return false;
    }

    $parent.addClass(OPENING);
    $submenu.css('zIndex', 9999);
    $submenu.fadeIn('fast', function () {
      $parent.removeClass(OPENING);
      $parent.addClass(OPENED);
    });
  }

  $.fn.westernDropdown = function () {
    return this.each(function () {
      var $menu_items = $(this).children('li');
      var $menu_submenu_parents = $menu_items.has('ul');
      var $menu_submenu_parent_links = $menu_submenu_parents.children('a');

      $menu_submenu_parents.click(click_submenu_parent);
      $menu_submenu_parents.mouseenter(mouseenter_submenu_parent);
      $menu_submenu_parents.mouseleave(mouseleave_submenu_parent);
      $menu_submenu_parent_links.click(click_submenu_parent_link);
    });
  };

}) (jQuery, this, this.document);
